/* BEGIN_HEADER                                                   TreeView 3
 *
 * Please refer to our LICENSE file if you wish to make changes to this software
 *
 * END_HEADER 
 */

import edu.stanford.genetics.treeview.app.LinkedViewApp;

/**
 * This class is a hollow shell that runs the package-scoped app.
 */
public class LinkedView {

	public static void main(final String astring[]) {

		LinkedViewApp.main(astring);
	}
}
