package edu.stanford.genetics.treeview;

public class ExportException extends Exception {

	private static final long serialVersionUID = 1L;

	public ExportException(final String string) {
		super(string);
	}

}
