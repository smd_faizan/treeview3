package edu.stanford.genetics.treeview;

public enum ViewType {
	WELCOME_VIEW, LOADERROR_VIEW, PROGRESS_VIEW, DENDRO_VIEW;
}
