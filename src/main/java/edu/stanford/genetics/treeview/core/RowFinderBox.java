/* BEGIN_HEADER                                                   TreeView 3
 *
 * Please refer to our LICENSE file if you wish to make changes to this software
 *
 * END_HEADER 
 */

package edu.stanford.genetics.treeview.core;

/**
 * @author aloksaldanha
 *
 */
public class RowFinderBox extends LabelFinderBox {

	/**
	 * @param f
	 * @param hI
	 * @param geneSelection
	 */
	public RowFinderBox() {

		super("Row");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.stanford.genetics.treeview.HeaderFinder#scrollToIndex(int)
	 */
	@Override
	public void scrollToIndex(final int i) {
	}

}
